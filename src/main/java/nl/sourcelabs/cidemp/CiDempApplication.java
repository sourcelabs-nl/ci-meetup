package nl.sourcelabs.cidemp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CiDempApplication {

	public static void main(String[] args) {
		SpringApplication.run(CiDempApplication.class, args);
	}

}
